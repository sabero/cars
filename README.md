# Cars #


![cars.jpg](https://bitbucket.org/repo/Kodk9X/images/3045651079-cars.jpg)


 This is an android application that:

    1. Renders a list of car models grouped by car make, consuming the JSON  feed provided.

    2. Show a detail view of each car model in a new view when tapped.




Resource: ​ http://sabero.hu/exampleCode/cars.json

Application: http://sabero.hu/exampleCode/cars.apk



# Experience #

* Understanding of design patterns and Object-Oriented Programming

* Working with Android Studio

* Using knowledge of Android SDK's

* Working with remote data via RESTfull API using JSON

* Using multi­layered architecture Model–view–presenter (MVP)

* Integrating, implementing and using third-party libraries and APIs Like: "ActiveAndroid" and "okhttp"

* Also working with GIT