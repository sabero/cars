package hu.sabero.examplecode.cars.presenter;

/**
 * Presenter/ controller interface which controle the "CarsActivity" bihaviors
 * Created by saber on 2016. 07. 11..
 */
public interface CarsPresenter extends Presenter {

    /**
     * Method to loads team list and their team member lists from the device database
     */
    void prepareCarListFromSQLiteDb();

    /**
     * Method to display team list and their team member lists
     */
    void showCarListOnScreen();

    /**
     * Method to navigate to team member detail screen
     * @param id: long selected team member ID
     */
    void navigateToCarModelDetail(long id);

}
