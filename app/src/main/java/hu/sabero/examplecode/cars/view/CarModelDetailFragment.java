package hu.sabero.examplecode.cars.view;

import android.app.Activity;
import android.content.Context;
import android.content.res.Configuration;
import android.graphics.Point;
import android.os.Bundle;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.v4.app.Fragment;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.TextView;

import hu.sabero.examplecode.cars.R;
import hu.sabero.examplecode.cars.model.CarModel;
import hu.sabero.examplecode.cars.presenter.CarModelDetailPresenter;
import hu.sabero.examplecode.cars.util.ImageFetcher;

/**
 * A fragment representing a single CarModel detail screen.
 * This fragment is either contained in a {@link CarsActivity}
 * in two-pane mode (on tablets) or a {@link CarModelDetailActivity}
 * on handsets.
 */
public class CarModelDetailFragment extends Fragment {

    private CarModel selectedCarModel;
    private ImageFetcher imageFetcher;
    private ImageView carModelImage;

    /**
     * The fragment argument representing the car model ID that this fragment
     * represents.
     */
    public static final String ARG_CAR_MODEL_ID = "selected_car_model_id";

    /**
     * Mandatory empty constructor for the fragment manager to instantiate the
     * fragment (e.g. upon screen orientation changes).
     */
    public CarModelDetailFragment() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments().containsKey("presenter")){
            CarModelDetailPresenter presenter = getArguments().getParcelable("presenter");
            presenter.prepareCarModelFromSQLiteDb();
            selectedCarModel = presenter.getSelectedCarModel();
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.car_model_detail, container, false);

        if (selectedCarModel != null){
            Activity activity = this.getActivity();
            CollapsingToolbarLayout appBarLayout = (CollapsingToolbarLayout) activity.findViewById(R.id.toolbar_layout);
            if (appBarLayout != null) {
                appBarLayout.setTitle(selectedCarModel.getName());
            }

            carModelImage = (ImageView) rootView.findViewById(R.id.car_model_image);
            ((TextView) rootView.findViewById(R.id.car_model_name)).setText(selectedCarModel.getName());
            ((TextView) rootView.findViewById(R.id.car_model_format)).setText(getString(R.string.format_label) + " " + selectedCarModel.getFormat());
            ((TextView) rootView.findViewById(R.id.car_model_position)).setText(getString(R.string.position_label) + " " + selectedCarModel.getPosition());
            ((TextView) rootView.findViewById(R.id.car_model_engine)).setText(getString(R.string.engine_label) + " " + selectedCarModel.getEngine());
            ((TextView) rootView.findViewById(R.id.car_model_size)).setText(getString(R.string.size_label) + " " + Double.toString(selectedCarModel.getSize()) + " ltr");
            ((TextView) rootView.findViewById(R.id.car_model_zero_62mph)).setText(getString(R.string.zero_62mph_label) + " " + Double.toString(selectedCarModel.getZero_62mph()) + " secs");
            ((TextView) rootView.findViewById(R.id.car_model_power)).setText(getString(R.string.power_label) + " " + Integer.toString(selectedCarModel.getPower()) + " bhp");
            ((TextView) rootView.findViewById(R.id.car_model_top_speed)).setText(getString(R.string.top_speed_label) + " " + Integer.toString(selectedCarModel.getTopSpeed()) + " mph");
            ((TextView) rootView.findViewById(R.id.car_model_price)).setText(getString(R.string.price_label) + " £" + Double.toString(selectedCarModel.getPrice()));
        }

        return rootView;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        // Use the parent activity to load the image asynchronously into the ImageView (so a single
        // cache can be used over all pages in the ViewPager
        if (CarModelDetailActivity.class.isInstance(getActivity())) {
            imageFetcher = ((CarModelDetailActivity) getActivity()).getImageFetcher();
            imageFetcher.loadImage(selectedCarModel.getImageURL(), carModelImage);
        }
        setResponsiveCarModelImage(carModelImage);
    }

    private void setResponsiveCarModelImage(ImageView imageView) {
        int x = (int) (getWindowSize(getActivity()).x);
        int y = (int) (getWindowSize(getActivity()).y);
        if(x < y){
            y = (int)(y * (0.40));
        }else{
            y = (int)(y * (1.30));
        }
        imageView.getLayoutParams().height = y;
        imageView.getLayoutParams().width = x;
    }

    /**
     * Method for getting device window height and width via Point object
     * @param context the application context
     * @return Point containing the width and height
     */
    public static Point getWindowSize(Context context) {
        WindowManager windowManager = (WindowManager) context.getSystemService(Context.WINDOW_SERVICE);
        Display display = windowManager.getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);
        return size;
    }

}
