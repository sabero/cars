package hu.sabero.examplecode.cars.presenter;

/**
 * Presenter interface
 * Created by saber on 2016. 07. 29..
 */
public interface Presenter {
    /**
     * Method to be imlements in child classes
     * @param result: the response String of the webservice
     */
    void callback(String result);
}
