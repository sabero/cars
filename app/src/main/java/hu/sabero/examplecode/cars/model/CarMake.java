package hu.sabero.examplecode.cars.model;

import android.provider.BaseColumns;

import com.activeandroid.Model;
import com.activeandroid.annotation.Column;
import com.activeandroid.annotation.Table;
import com.activeandroid.query.Select;

import java.util.List;

/**
 * Class represents the model of "CarMake" objects and "team" datatable
 * Created by saber on 2016. 07. 11..
 */
@Table(name = "car_make", id = BaseColumns._ID)
public class CarMake extends Model{

    /**
     * Variable/ attribute represents the car make name
     */
    @Column(name = "name")
    private String name;

    /**
     * Variable/ attribute represents the URL of the car make logo
     */
    @Column(name = "logo_url")
    private String logoURL;

    /**
     * Default constructor without parameters
     */
    public CarMake(){
        super();
    }

    /**
     * Method to query the car make name
     * @return name: String
     */
    public String getName() {
        return name;
    }

    /**
     * Method to modify the car make name
     * @param name: String
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * Method to query the car make logo URL
     * @return logo URL: String
     */
    public String getLogoURL() {
        return logoURL;
    }

    /**
     * Method to modify the car make logo URL
     * @param logoURL: String
     */
    public void setLogoURL(String logoURL) {
        this.logoURL = logoURL;
    }

    /**
     * Method to query all car make objects from SQLite database, car_make table
     * @return List<CarMake>
     */
    public static List<CarMake> all(){
        return  new Select().from(CarMake.class).execute();
    }

    /**
     * Method to query all car models of the car make from SQLite database, car_list_model table
     * @return
     */
    public List<CarModel> models(){
        return getMany(CarModel.class, "car_make");
    }

    /**
     * Method allows to update the states of car make objects
     *
     */
    public void  update(String name, String logoURL){
        this.name = name;
        this.logoURL = logoURL;
        this.save();
    }
}
