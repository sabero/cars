package hu.sabero.examplecode.cars.presenter;

import android.annotation.SuppressLint;
import android.os.Parcel;

import hu.sabero.examplecode.cars.CarsApplication;
import hu.sabero.examplecode.cars.model.CarModel;
import hu.sabero.examplecode.cars.view.CarModelDetailView;

/**
 * Class implements "CarModelDetailPresenter" interface
 * Created by saber on 2016. 08. 01..
 */
@SuppressLint("ParcelCreator")
public class CarModelDetailPresenterImpl implements CarModelDetailPresenter {

    /**
     * Selected team member
     */
    private CarModel selectedCarModel;

    private CarModelDetailView carModelDetailView;

    public CarModelDetailPresenterImpl(CarModelDetailView carModelDetailView) {
        this.carModelDetailView = carModelDetailView;
    }

    @Override
    public void prepareCarModelFromSQLiteDb() {
        selectedCarModel = CarModel.load(CarModel.class, CarsApplication.getSelectedCarModelId());
    }

    @Override
    public CarModel getSelectedCarModel() {
        return selectedCarModel;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {

    }
}
