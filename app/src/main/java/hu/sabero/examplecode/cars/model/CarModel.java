package hu.sabero.examplecode.cars.model;

import android.provider.BaseColumns;

import com.activeandroid.Model;
import com.activeandroid.annotation.Column;
import com.activeandroid.annotation.Table;

/**
 * Class represents the model of "CarModel" objects and "car_list_model" datatable
 * Created by saber on 2016. 07. 11..
 */
@Table(name = "car_model", id = BaseColumns._ID)
public class CarModel extends Model{


    /**
     * Variable/ attribute represents the name of the car momdel
     */
    @Column(name = "name")
    private String name;

    /**
     * Variable/ attribute represents the format of the car momdel
     */
    @Column(name = "format")
    private String format;

    /**
     * Variable/ attribute represents the position of the car momdel
     */
    @Column(name = "position")
    private String position;

    /**
     * Variable/ attribute represents the engine of the car momdel
     */
    @Column(name = "engine")
    private String engine;

    /**
     * Variable/ attribute represents the size of the car momdel
     */
    @Column(name = "size")
    private double size;

    /**
     * Variable/ attribute represents the acceleration 0-62 mph of the car momdel
     */
    @Column(name = "zero_62mph")
    private double zero_62mph;

    /**
     * Variable/ attribute represents the power of the car momdel
     */
    @Column(name = "power")
    private int power;

    /**
     * Variable/ attribute represents the top speed of the car momdel
     */
    @Column(name = "top_speed")
    private int topSpeed;

    /**
     * Variable/ attribute represents the price of the car momdel
     */
    @Column(name = "price")
    private double price;

    /**
     * Variable/ attribute represents the car momdel image
     */
    @Column(name = "image_url")
    private String imageURL;

    /**
     * Variable/ attribute represents which car make the car model belongs
     */
    @Column(name = "car_make", onUpdate = Column.ForeignKeyAction.CASCADE, onDelete = Column.ForeignKeyAction.CASCADE)
    private CarMake carMake;

    /**
     * Method to query the name of the car momdel
     * @return
     */
    public String getName() {
        return name;
    }

    /**
     * Method to modify the name of the car momdel
     * @param name: String
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * Method to query the format of the car momdel
     * @return
     */
    public String getFormat() {
        return format;
    }

    /**
     * Method to modify the format of the car momdel
     * @param format: String
     */
    public void setFormat(String format) {
        this.format = format;
    }

    /**
     * Method to query the position of the car momdel
     * @return
     */
    public String getPosition() {
        return position;
    }

    /**
     * Method to modify the position of the car momdel
     * @param position: String
     */
    public void setPosition(String position) {
        this.position = position;
    }

    /**
     * Method to query the engine of the car momdel
     * @return
     */
    public String getEngine() {
        return engine;
    }

    /**
     * Method to modify the engine of the car momdel
     * @param engine: String
     */
    public void setEngine(String engine) {
        this.engine = engine;
    }

    /**
     * Method to query the size of the car momdel
     * @return
     */
    public double getSize() {
        return size;
    }

    /**
     * Method to modify the size of the car momdel
     * @param size: double
     */
    public void setSize(double size) {
        this.size = size;
    }

    /**
     * Method to query the acceleration 0-62 mph of the car momdel
     * @return
     */
    public double getZero_62mph() {
        return zero_62mph;
    }

    /**
     * Method to modify the acceleration 0-62 mph of the car momdel
     * @param zero_62mph: double
     */
    public void setZero_62mph(double zero_62mph) {
        this.zero_62mph = zero_62mph;
    }

    /**
     * Method to query the power of the car momdel
     * @return
     */
    public int getPower() {
        return power;
    }

    /**
     * Method to modify the power of the car momdel
     * @param power: int
     */
    public void setPower(int power) {
        this.power = power;
    }

    /**
     * Method to query the top speed of the car momdel
     * @return
     */
    public int getTopSpeed() {
        return topSpeed;
    }

    /**
     * Method to modify the top speed of the car momdel
     * @param topSpeed: int
     */
    public void setTopSpeed(int topSpeed) {
        this.topSpeed = topSpeed;
    }

    /**
     * Method to query the price of the car momdel
     * @return
     */
    public double getPrice() {
        return price;
    }

    /**
     * Method to modify the price of the car momdel
     * @param price: double
     */
    public void setPrice(double price) {
        this.price = price;
    }

    /**
     * Method to query the car momdel image URL
     * @return image: String
     */
    public String getImageURL() {
        return imageURL;
    }

    /**
     * Method to modify the car momdel image
     * @param imageURL: String
     */
    public void setImageURL(String imageURL) {
        this.imageURL = imageURL;
    }

    /**
     * Method to query the care make of the car momdel
     * @return carMake: CarMake
     */
    public CarMake getCarMake() {
        return carMake;
    }

    /**
     * Method to modify the car make of the car momdel
     * @param carMake: CarMake
     */
    public void setCarMake(CarMake carMake) {
        this.carMake = carMake;
    }

    /**
     * Default constructor without parameters
     */
    public CarModel(){
        super();
    }


    /**
     * Method allows to update the states of car model objects
     * @param carModel: CarModel
     */
    public void  update(CarModel carModel){
        this.name = carModel.getName();
        this.format = carModel.getFormat();
        this.position = carModel.getPosition();
        this.engine = carModel.getEngine();
        this.size = carModel.getSize();
        this.zero_62mph = carModel.getZero_62mph();
        this.power = carModel.getPower();
        this.topSpeed = carModel.getTopSpeed();
        this.price = carModel.getPrice();
        this.imageURL = carModel.getImageURL();
        this.save();
    }
}
