package hu.sabero.examplecode.cars;

import android.content.Context;

import com.activeandroid.ActiveAndroid;

/**
 * Created by saber on 2016. 07. 11..
 */
public class CarsApplication extends com.activeandroid.app.Application {

    private static Context appContext;
    private static String webServiceUrl;
    private static boolean offlineMode;
    private static long selectedCarModelId;

    @Override
    public void onCreate() {
        super.onCreate();
        appContext = this;
        webServiceUrl = getString(R.string.web_service_url);
        ActiveAndroid.initialize(this);
    }

    public static Context getContext() {
        return appContext;
    }

    public static String getWebServiceUrl(){
        return webServiceUrl;
    }

    public static boolean isOfflineMode() {
        return offlineMode;
    }

    public static void setOfflineMode(boolean offlineMode) {
        CarsApplication.offlineMode = offlineMode;
    }

    public static long getSelectedCarModelId() {
        return selectedCarModelId;
    }

    public static void setSelectedCarModelId(long selectedCarModelId) {
        CarsApplication.selectedCarModelId = selectedCarModelId;
    }
}
