package hu.sabero.examplecode.cars.presenter;

import android.content.Intent;
import android.support.annotation.NonNull;
import android.view.View;
import android.widget.ExpandableListView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import hu.sabero.examplecode.cars.CarsApplication;
import hu.sabero.examplecode.cars.adapter.ExpandableListAdapter;
import hu.sabero.examplecode.cars.model.CarMake;
import hu.sabero.examplecode.cars.model.CarModel;
import hu.sabero.examplecode.cars.view.CarModelDetailActivity;
import hu.sabero.examplecode.cars.view.CarModelDetailFragment;
import hu.sabero.examplecode.cars.view.CarsView;

/**
 * Created by saber on 2016. 07. 11..
 */
public class CarsPresenterImpl implements CarsPresenter {

    private ExpandableListView expandableListView;
    private List<String> listDataHeader;
    private HashMap<String, List<CarModel>> listDataChild;
    private CarsView carsView;


    public CarsPresenterImpl(CarsView carsView, @NonNull ExpandableListView expandableListView) {
        this.carsView = carsView;
        this.expandableListView = expandableListView;
    }

    @Override
    public void prepareCarListFromSQLiteDb() {
        listDataHeader = new ArrayList<String>();
        listDataChild = new HashMap<String, List<CarModel>>();
        List<CarMake> carMakes = CarMake.all();

        for (int i=0; i < carMakes.size(); i++){
            CarMake carMake = carMakes.get(i);
            listDataHeader.add(carMake.getName());
            listDataChild.put(carMake.getName(), carMake.models());
        }

    }

    @Override
    public void showCarListOnScreen() {

        // setting list adapter
        expandableListView.setAdapter(new ExpandableListAdapter(CarsApplication.getContext(), listDataHeader, listDataChild, carsView.getImageFetcher()));

        // Listview Group click listener
        expandableListView.setOnGroupClickListener(new ExpandableListView.OnGroupClickListener() {

            @Override
            public boolean onGroupClick(ExpandableListView parent, View v,
                                        int groupPosition, long id) {
                return false;
            }
        });

        // Listview Group expanded listener
        expandableListView.setOnGroupExpandListener(new ExpandableListView.OnGroupExpandListener() {

            @Override
            public void onGroupExpand(int groupPosition) {
                Toast.makeText(CarsApplication.getContext(),
                        listDataHeader.get(groupPosition) + " Expanded",
                        Toast.LENGTH_SHORT).show();
            }
        });

        // Listview Group collasped listener
        expandableListView.setOnGroupCollapseListener(new ExpandableListView.OnGroupCollapseListener() {

            @Override
            public void onGroupCollapse(int groupPosition) {
                Toast.makeText(CarsApplication.getContext(),
                        listDataHeader.get(groupPosition) + " Collapsed",
                        Toast.LENGTH_SHORT).show();

            }
        });

        // Listview on child click listener
        expandableListView.setOnChildClickListener(new ExpandableListView.OnChildClickListener() {

            @Override
            public boolean onChildClick(ExpandableListView parent, View v,
                                        int groupPosition, int childPosition, long id) {
                CarModel carModel = (CarModel) listDataChild.get(listDataHeader.get(groupPosition)).get(childPosition);
                final String carModelName = carModel.getName();
                // TODO Auto-generated method stub
                Toast.makeText(
                        CarsApplication.getContext(),
                        listDataHeader.get(groupPosition)
                                + " : "
                                + carModelName, Toast.LENGTH_SHORT)
                        .show();
                CarsApplication.setSelectedCarModelId(carModel.getId());
                navigateToCarModelDetail(carModel.getId());
                return false;
            }
        });
    }

    @Override
    public void navigateToCarModelDetail(long id) {
        Intent intent = new Intent(carsView.getContext(), CarModelDetailActivity.class);
        intent.putExtra(CarModelDetailFragment.ARG_CAR_MODEL_ID, id);
        carsView.getContext().startActivity(intent);
    }


    @Override
    public void callback(String result) {

    }
}
