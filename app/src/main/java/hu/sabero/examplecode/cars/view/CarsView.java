package hu.sabero.examplecode.cars.view;

import android.content.Context;

import hu.sabero.examplecode.cars.util.ImageFetcher;

/**
 * Created by saber on 2016. 07. 11..
 */
public interface CarsView extends MainView {
    ImageFetcher getImageFetcher();
}
