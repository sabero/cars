package hu.sabero.examplecode.cars.view;

import android.content.Context;
import android.graphics.Point;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.view.Display;
import android.view.View;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.ImageView;

import hu.sabero.examplecode.cars.CarsApplication;
import hu.sabero.examplecode.cars.R;
import hu.sabero.examplecode.cars.presenter.SplashPresenter;
import hu.sabero.examplecode.cars.presenter.SplashPresenterImpl;

/**
 * Created by saber on 2016. 07. 11..
 */
public class SplashActivity extends AppCompatActivity implements MainView {

    private Animation cotinueButtonAnim;
    private SplashPresenter presenter;
    private ImageView carsLogo;
    private static final int SPLASH_DURATION = 3000;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        setResponsiveSplashLogo();
        presenter = new SplashPresenterImpl(this);
        View view = findViewById(R.id.continue_button);
        Button continueButton = (Button) view;
        presenter.checkNetwork();
        if(CarsApplication.isOfflineMode()) {
            if(continueButton.getVisibility() != View.VISIBLE)
                continueButton.setVisibility(View.VISIBLE);
            cotinueButtonAnim = AnimationUtils.loadAnimation(this, R.anim.button_animation);
            continueButton.setAnimation(cotinueButtonAnim);
            continueButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    v.startAnimation(cotinueButtonAnim);
                    presenter.navigateToCarsView(SplashActivity.this);
                    finish();
                }
            });
        }else{
            if(continueButton.getVisibility() == View.VISIBLE)
                continueButton.setVisibility(View.INVISIBLE);
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    presenter.callSaberoWebService(presenter);
                }
            }, SPLASH_DURATION);

        }
    }

    /**
     * Method to settings logo width value
     */
    private void setResponsiveSplashLogo() {
        carsLogo = (ImageView)findViewById(R.id.cars_logo);
        carsLogo.getLayoutParams().width = (int) (getWindowSize(this).x * 0.75);
    }

    /**
     * Method for getting device window height and width via Point object
     * @param context the application context
     * @return Point containing the width and height
     */
    public static Point getWindowSize(Context context) {
        WindowManager windowManager = (WindowManager) context.getSystemService(Context.WINDOW_SERVICE);
        Display display = windowManager.getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);
        return size;
    }

    @Override
    public Context getContext() {
        return this;
    }
}

